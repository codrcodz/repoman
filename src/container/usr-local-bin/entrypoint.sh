#!/usr/bin/env bash

repoman_usage()
{
  echo "Usage: repoman"
  echo "  decrypt-secrets | encrypt-secrets | "
  echo "  deploy-configs  | dryrun-deploy     "
  echo "Subcommands:"
  echo "  decrypt-secrets:    decrypt secrets in encrypted directory and stage in workdir directory for editing"
  echo "  encrypt-secrets:    encrypt secrets in workdir directory and stage for use by other repoman subcommands"
  echo "  deploy-configs:     deploy configuration settings in gitlabform-formatted config.yml file"
  echo "  dryrun-deploy:      dryrun a configuration deploy using gitlabform's noop feature"
}

{
  if [[ -z "$GITLAB_TOKEN" ]]; then
    if [[ -f "${GITLAB_TOKEN_FILE:=/run/secrets/GITLAB_TOKEN}" ]]; then
      read -r GITLAB_TOKEN < "${GITLAB_TOKEN_FILE}"
      export GITLAB_TOKEN
    else
      echo -e "\n[FAIL] Failed to find GITLAB_TOKEN or GITLAB_TOKEN_FILE; exiting.\n" 1>&2
      exit 1
    fi
  else
    export GITLAB_TOKEN
  fi &&
  if [[ "${#GITLAB_TOKEN}" == "0" ]]; then
    echo -e "\n[FAIL] GITLAB_TOKEN variable is empty; exiting." 1>&2
    exit 1
  fi &&
  if [[ -f "${ANSIBLE_VAULT_PASSWORD_FILE:=/run/secrets/ANSIBLE_VAULT_PASS}" ]]; then
    export ANSIBLE_VAULT_PASSWORD_FILE
  else
    echo -e "\n[FAIL] Failed to find valid ansible vault password file; exiting.\n" 1>&2
    exit 1
  fi &&
  if [[ ! -s "${ANSIBLE_VAULT_PASSWORD_FILE}" ]]; then
    echo -e "\n[FAIL] ANSIBLE_VAULT_PASSWORD_FILE is empty; exiting." 1>&2
    exit 1
  fi &&
  case "$1" in
  encrypt-secrets)
    ansible-playbook /etc/ansible/encrypt-secrets.yml
    ;;
  decrypt-secrets)
    ansible-playbook /etc/ansible/decrypt-secrets.yml
    ;;
  deploy-configs)
    ansible-playbook /etc/ansible/deploy-configs.yml
    ;;
  dryrun-deploy)
    ansible-playbook /etc/ansible/deploy-configs.yml -e "dryrun=true"
    ;;
  *)
    echo -e "\n[FAIL] Failed to pass a valid subcommand to repoman; exiting.\n" 1>&2
    repoman_usage
    exit 1
    ;;
  esac
} ||
  {
    echo -e "\n[FAIL] Subcommand returned a non-zero exit code; exiting.\n" 1>&2
    exit 1
  }
