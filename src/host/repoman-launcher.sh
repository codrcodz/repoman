#!/usr/bin/env bash

# @name repoman-launcher.sh
# @brief A helper script for launching a local instance of repoman
# @description
#    This script is used to launch a local instance of repoman.
#    Common use cases for wanting to launch repoman locally include:
#        - Encrypting secrets files for inclusion in a repoman-config repository
#        - Decrypting secrets files from an existing repoman-config repository for reading/modification
#        - Manually rotating the ansible-vault password file used to encrypt/decrypt secrets
#        - Manually rotating deploy keypairs associated with a remote repository
#        - Creating and applying repository configuration-as-code from your local machine to remote repositories

# @description
#     This function ensures that the user is not executing the script as root or with sudo.
# @noargs
# @exitcode 0 If user is non-root.
# @exitcode 1 If user is root or effectively root (via sudo).
no_root() {
  if [[ "$EUID" == "0" ]] || [[ -n "$SUDO_USER" ]]; then
    echo -e "\n[FAIL] Script must not be run as root or with sudo; exiting." 1>&2
    exit 1
  fi
}

# @description
#    The bootstrap funtion sets the 'scriptdir' variable for other functions to use.
#    It will also 'cd' into that directory to ensure all references to files/dirs are consistent.
# @arg $0 string The filename of the script, as called by the user
# @exitcode 0 If successful
# @exitcode 1 If failed; does not 'return 1', fatal errors 'exit 1', exiting script entirely
bootstrap() {
  {
    if [[ -z "$tld" ]]; then
      echo -e "\n[FAIL] Parent shell/process failed to export \$tld variable to script; exiting.\n" 1>&2
      exit 1
    fi
    unset scriptdir &&
      declare -g scriptdir &&
      scriptdir="$(dirname "$(realpath "$0")")" &&
      readonly scriptdir &&
      if [[ -n "$scriptdir" ]] && [[ -d "$scriptdir" ]]; then
        cd "$scriptdir"
      else
        echo -e "\n[FAIL] Failed to detect script directory; exiting.\n" 1>&2
        exit 1
      fi
  } ||
    {
      echo -e "\n[FAIL] Failed to change directories into $0 script dir; exiting.\n" 1>&2
      exit 1
    }
  {
    unset scriptname &&
      declare -g scriptname &&
      scriptname="$(basename "$0")" &&
      readonly scriptname &&
      if [[ ! -f "$scriptname" ]]; then
        echo -e "\n[FAIL] Failed to detect scriptname; exiting.\n" 1>&2
        exit 1
      fi
  }
}

# @description
#    This function performs a preliminary dependency check.
#    Any non-bash-builtin dep in the path should be added to this function.
# @noargs
# @exitcode 0 If all listed deps are present in $PATH
# @exitcode 1 If any listed dep is not present in $PATH; script exits after checking all.
dep_check() {
  {
    local missing_deps &&
      missing_deps=0 &&
      for dep in mv \
        realpath \
        basename \
        dirname \
        id \
        mkdir \
        pwgen \
        docker \
        docker-compose; do
        hash $dep ||
          {
            echo -e "\n[WARN] $dep not found in path." 1>&2
            missing_deps=1
          }
      done
    if [[ "${missing_deps}" == "1" ]]; then
      echo -e "\n[FAIL] Please install missing dependencies and try again; exiting." 1>&2
      exit 1
    fi
  }
}

# @description
#    This function creates (or checks for) required credentials and configs.
# @exitcode 0 If all configs and creds are present
# @exitcode 1 If any configs or creds are missing and cannot be generated
init() {
  local init_fails &&
    init_fails=0 &&
    if [[ ! -d "$tld/repoman-data/configs/" ]]; then
      echo -e "\n[WARN] Failed to find repoman-data configs directory ($tld/repoman-data/configs/)." 1>&2
      echo "         To fix: cd $tld && \ "
      echo "                 git clone https://yourgitserver.com/yourorg/repoman-data.git repoman-data"
      echo "                 OR"
      echo "                 mkdir -p $tld/repoman-data/configs"
      echo "         See README.md for more information."
      init_fails=1
    fi &&
    if [[ ! -d "$tld/repoman-data/secrets/" ]]; then
      echo -e "\n[WARN] Failed to find repoman-data secrets directory ($tld/repoman-data/secrets/)." 1>&2
      echo "         To fix: cd $tld && \ "
      echo "                 git clone https://yourgitserver.com/yourorg/repoman-data.git repoman-data"
      echo "                 OR"
      echo "                 mkdir -p $tld/repoman-data/configs"
      echo "         See README.md for more information."
      init_fails=1
    fi &&
    if [[ ! -d "$HOME/.config/repoman/" ]]; then
      {
        mkdir -p "$HOME"/.config/repoman/ &&
          chmod 0700 "$HOME"/.config/repoman/
      } ||
        {
          echo -e "\n[WARN] Failed to create repoman credentials directory (~/.config/repoman/)." 1>&2 &&
            init_fails=1
        }
    fi &&
    if [[ ! -s "$HOME/.config/repoman/GITLAB_TOKEN" ]]; then
      echo -e "\n[WARN] Failed to find repoman gitlab credentials (~/.config/repoman/GITLAB_TOKEN)." 1>&2
      echo "         To fix: - Login to your Gitlab instance"
      echo "                 - Click on your profile in top-left corner"
      echo "                 - Click on your Access Tokens on left-side menu"
      echo "                 - Create a token with appropriate write permissions and expiration"
      echo "                 - Place token value in ~/.config/repoman/GITLAB_TOKEN file"
      echo "                 - Ensure file's permissions are set properly to protect the token"
      echo "         See README.md for more information."
      init_fails=1
    fi &&
    if [[ ! -s "$HOME/.config/repoman/ANSIBLE_VAULT_PASS" ]]; then
      {
        local temppassvar &&
          temppassvar="$RANDOM" &&
          until [[ "${#testpassvar}" -gt "32" ]]; do
            testpassvar+="${testpassvar}${RANDOM}"
          done &&
          echo "$temppassvar" > "$HOME"/.config/repoman/ANSIBLE_VAULT_PASS
      } ||
        {
          echo -e "\n[WARN] Failed to create repoman ansible-vault credentials (~/.config/repoman/ANSIBLE_VAULT_PASS)." 1>&2
          echo "         To fix: - Randomly generate a secure password"
          echo "                 - Place the password in the ~/.config/repoman/ANSIBLE_VAULT_PASS file"
          echo "                 - Ensure file's permissions are set properly to protect the password"
          echo "         See README.md for more information."
          init_fails=1
        }
    fi &&
    if [[ "$init_fails" == "1" ]]; then
      echo -e "\n[FAIL] Failed to initialize repoman; see errors above; exiting." 1>&2
      exit 1
    fi
}

# @description
#    This function builds and launches the user-selected environment.
#    It either launches a development or local production environment.
# @arg $1 string Is the environment name. This is optional. The default is "local".
build_and_launch() {
  if [[ -n "$1" ]]; then
    env_name="$1"
  else
    env_name="local"
  fi &&
    case "$env_name" in
    [Dd]ev | [Dd]evel | [Dd]evelop | [Dd]evelopment | repoman_devel)
      {
        docker-compose -f "$scriptdir/docker-compose.yml" build \
          --no-cache \
          --build-arg LOCAL_USER="$(id -un)" \
          --build-arg LOCAL_UID="$(id -u)" \
          --build-arg LOCAL_GROUP="$(id -gn)" \
          --build-arg LOCAL_GID="$(id -g)" \
          repoman_devel &&
          docker-compose -f "$scriptdir/docker-compose.yml" run --rm repoman_devel
      } ||
        {
          echo -e "\n[FAIL] Failed to build or launch repoman_devel env; exiting.\n" 1>&2
          exit 1
        }
      ;;
    [Ll]ocal | [Pp]roduction | [Pp]rod | repoman_local)
      {
        docker-compose -f "$scriptdir/docker-compose.yml" build \
          --build-arg LOCAL_USER="$(id -un)" \
          --build-arg LOCAL_UID="$(id -u)" \
          --build-arg LOCAL_GROUP="$(id -gn)" \
          --build-arg LOCAL_GID="$(id -g)" \
          repoman_local &&
          docker-compose -f "$scriptdir/docker-compose.yml" run --rm repoman_local
      } ||
        {
          echo -e "\n[FAIL] Failed to build or launch repoman_local env; exiting.\n" 1>&2
          exit 1
        }
      ;;
    *)
      echo -e "\n[FAIL] Unrecognized environment ($1); exiting.\n" 1>&2
      exit 1
      ;;
    esac
}

# @description
#    This function is the entrypoint to the script.
#    It builds either:
#        - repoman_devel (used for local development workflow)
#        - repoman_local (used for local production workflow)
# @arg $1 string If "dev" (or a variation of it), script will launch a development instance, not a local production one
# @exitcode 0 If all functions called by main() are successful and return 0.
# @exitcode 1 If any function fails; functions should exit the script on fatal errors. Main exits 1 if unhandled errors encountered.
main() {
  no_root &&
    dep_check &&
    bootstrap &&
    init &&
    build_and_launch "$1"
}

# This block calls main().
# The 'activate' script sanitizes $1 before passing to this one.
main "$1" ||
  {
    echo -e "\n[FAIL] Script failed with unhandled error; exiting." 1>&2
    exit 1
  }
