# mk-test.sh

This helper scripts assist repoman developers in writing new bashtest tests

## Overview

This script assists repoman developers with writing tests for `bashtest` to parse.
In addition to readable text, `bashtest` also check non-printable characters when running tests.
This can make writing and troubleshooting these tests very tedious and frustrating.
This script seeks to solve that problem by automatating the process.
As a developer, once you are happy with a repoman feature's behavior, use this to write your tests.
It should be used to call things like tree, md5sum, etc to check a desired end state after a repoman run.

## Index

* [self_heal()](#self_heal)
* [bootstrap()](#bootstrap)
* [dep_check()](#dep_check)
* [main()](#main)

### self_heal()

This function autoformats, lints, and generates docs for the script.
The autoformatter rules are configured with the .editorconfig in the tld of this repo.

#### Arguments

* **$0** (string): Tells the function the location of the script so each tool can parse it.

#### Exit codes

* **0**: If autoformatting, linting, and autodoc generation all pass.
* **1**: Once the first one fails. No warnings (return 1). Fatal errors (exit 1).

#### See also

* [https://github.com/reconquest/shdoc#features](#httpsgithubcomreconquestshdocfeatures)
* [https://github.com/koalaman/shellcheck#how-to-use](#httpsgithubcomkoalamanshellcheckhow-to-use)
* [https://github.com/mvdan/sh](#httpsgithubcommvdansh)
* [https://editorconfig.org/](#httpseditorconfigorg)

### bootstrap()

The bootstrap funtion sets the 'scriptdir' variable for other functions to use.
It will also 'cd' into that directory to ensure all references to files/dirs are consistent.

#### Arguments

* **$0** (string): The filename of the script, as called by the user

#### Exit codes

* **0**: If successful
* **1**: If failed; does not 'return 1', fatal errors 'exit 1', exiting script entirely

### dep_check()

This function performs a preliminary dependency check.
Any non-bash-builtin dep in the path should be added to this function.

_Function has no arguments._

#### Exit codes

* **0**: If all listed deps are present in $PATH
* **1**: If any listed dep is not present in $PATH; script exits immediately.

### main()

This function is the entrypoint to the script.
It moves old bashtests to $2.bak to avoid overwriting existing tests.
It replaces it with a file at $2 of the same name that starts with: '$ ' on line 1.
It appends '$1\n' where $cmd is the command `bashtest` will run.
It then runs the command and appends the output to the file, starting on line 2.
The resulting test file should pass if `bashtest` is run using it as input later.
***Warning***: This script will actually run the command passed to it as $1.
***Warning***: This script uses the `eval` builtin, so be careful and check your inputs!

#### Example

```bash
   ./mk-test.sh "echo 'test'" ./test.bashtest
@stdout
   [INFO] Creating bashtest file:
          ./test.bashtest
```

#### Arguments

* **$1** (string): This string should be double-quoted, and should contain the command you'd like bashtest to run.
* **$2** (string): This arg should be the desired file path where the script will place your bashtest.

#### Exit codes

* **0**: If bashtest file generation is successful.
* **1**: If bashtest file generation (or any prelimnary steps) fail. Exits, does not 'return 1'.

#### See also

* [https://github.com/pahaz/bashtest#typical-use-case](#httpsgithubcompahazbashtesttypical-use-case)

