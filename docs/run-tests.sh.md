# run-tests.sh

This script acts as a test framework for repoman

## Overview

This test framework uses docker-compose to run various test scenarios.
Tests environments are defined in the "test_env.yml" file in each test scenario directory.
The "test_env.yml" file is a docker-compose file. Secrets are defined in it as well.
Also included is a "mk-test.sh" script that aids the user in automatic test creation.
This framework uses tests written in bashtest format.
The bashtest perform functional evaluation and integration testing, NOT unit testing.
Configuration files mounted by docker-compose are stored in each test_* directory.
These directories also contain a bashtest/ subdir with a bashtest per file.

## Index

* [self_heal()](#self_heal)
* [bootstrap()](#bootstrap)
* [reset()](#reset)
* [prepare()](#prepare)
* [destroy()](#destroy)
* [run_test()](#run_test)
* [test_dep_check()](#test_dep_check)
* [build_container()](#build_container)
* [dep_check()](#dep_check)
* [main()](#main)

### self_heal()

This function autoformats, lints, and generates docs for this script (and others).
The autoformatter rules are configured with the .editorconfig in the tld of this repo.

#### Arguments

* **$0** (string): Tells the function the location of the script so each tool can parse it.

#### Exit codes

* **0**: If autoformatting, linting, and autodoc generation all pass.
* **1**: Once the first one fails. No warnings (return 1). Fatal errors (exit 1).

#### See also

* [https://github.com/reconquest/shdoc#features](#httpsgithubcomreconquestshdocfeatures)
* [https://github.com/koalaman/shellcheck#how-to-use](#httpsgithubcomkoalamanshellcheckhow-to-use)
* [https://github.com/mvdan/sh](#httpsgithubcommvdansh)
* [https://editorconfig.org/](#httpseditorconfigorg)

### bootstrap()

The bootstrap funtion sets the 'scriptdir' variable for other functions to use.
It will also 'cd' into that directory to ensure all references to files/dirs are consistent.

#### Arguments

* **$0** (string): The filename of the script, as called by the user

#### Exit codes

* **0**: If successful
* **1**: If failed; does not 'return 1', fatal errors 'exit 1', exiting script entirely

### reset()

This function resets the test build directories prior to each test run.
It does not perform any of this work itself.
It loops through the test scenario dirnames and passes test numbers to the 'destroy' func.

_Function has no arguments._

#### See also

* [destroy()](#destroy)

### prepare()

This function prepares the build environments used to run tests.
Each test run, for each scenario, gets its own environment and configs.
Environments consist of the entry in "test_env.yml" file
Configs come from the contents of "tempdir/", "configs/", and "secrets/" subdirs.
If the subdirs are not already present in the test_* dir, they will be created.
This ensures each test env has /etc/repoman/{configs,secrets,tempdir} mounted.

#### Arguments

* **$1** (integer): Is the number of the test scenario being prepared.

#### Exit codes

* **0**: If successful.
* **1**: If failed; exits, does not return.

#### See also

* [bootstrap()](#bootstrap)

### destroy()

This function destroys test build directories.
It is called by reset() when a new test run begins.
It is called by run_test() when a test scenario passes.
It is skipped by passing the "--no-destroy" function to main().

#### Arguments

* **$1** (integer): Is the test scenario number whose build dir will be removed.

#### Exit codes

* **0**: If successful.
* **1**: If failed. Exits on fail, does not return.

#### See also

* [reset()](#reset)
* [run_test()](#run_test)
* [main()](#main)

### run_test()

This function performs most of the logic and flow of the script.
It calls prepare() to prep test environments.
It executes docker-compose once environments are prepped/resent.
It runs `bashtest` once docker-compose exits (if successful).
It calls destroy() to cleanup environments of successful tests.
It skips cleanup if main() passes "--no-destroy" or a test fails.
When failures occur, this function warns and returns, but does not exit.
This gives other tests a chance to run, instead of the script exiting on first test failure.

#### Arguments

* **$1** (integer): Is the test scenario number to be executed.

#### Exit codes

* **0**: If all bashtests pass for a given test scenario
* **1**: If any bashtests with a scenario fail, or test scenario is not found; returns, does not exit.

#### See also

* [prepare()](#prepare)
* [destroy()](#destroy)
* [main()](#main)
* [https://github.com/pahaz/bashtest#typical-use-case](#httpsgithubcompahazbashtesttypical-use-case)
* [https://docs.docker.com/compose/](#httpsdocsdockercomcompose)

### test_dep_check()

This function performs a test case dependency check.
Any non-bash-builtin test deps in the path should be added to a test_*/bashtests/deps.txt file.
This function is called by run_test() on a per-test basis.
The "|| [[ -n "$dep ]]" code is there for instances where the deps file does not end in a new line.

#### Arguments

* 1 integer Is the test number to check the test deps file for

#### Exit codes

* **0**: If all listed test deps are present in $PATH
* **1**: If any listed test dep is not present in $PATH; script will 'return 1'.

#### See also

* [run_test()](#run_test)
* [https://stackoverflow.com/questions/12916352/shell-script-read-missing-last-line/12919766#12919766](#httpsstackoverflowcomquestions12916352shell-script-read-missing-last-line1291976612919766)

### build_container()

This function builds the docker containers for the test environments.
It uses a multi-stage build process to build the environment, from the same Dockerfile.
When the user passes the "all" option to main(), these are built without using the cache.
This ensures that when repoman dev's run their final tests, no old configs are cached in the test images.
When repoman devs pass a test number to main(), cached image builds are used to speed up dev/test work.

#### Arguments

* **$1** (string): Is the "--no-cache" flag selectively passed from main()

#### Exit codes

* **0**: If succcessful.
* **1**: If failed. Exits on failed builds; does not return 1 to main().

#### See also

* [https://docs.docker.com/engine/reference/builder/](#httpsdocsdockercomenginereferencebuilder)
* [https://docs.docker.com/compose/reference/build/](#httpsdocsdockercomcomposereferencebuild)
* [https://docs.docker.com/develop/develop-images/multistage-build/](#httpsdocsdockercomdevelopdevelop-imagesmultistage-build)
* [https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#leverage-build-cache](#httpsdocsdockercomdevelopdevelop-imagesdockerfile_best-practicesleverage-build-cache)

### dep_check()

This function performs a preliminary dependency check.
Any non-bash-builtin dep in the path should be added to this function.

_Function has no arguments._

#### Exit codes

* **0**: If all listed deps are present in $PATH
* **1**: If any listed dep is not present in $PATH; script exits after checking all.

### main()

This function is the main entrypoint into the script.
It calls dep_check() to ensure required deps are installed.
It calls self_heal() to perform auto-linting/formatting/docs.
It calls bootstrap() to setup global vars and cd into the right directory.
It calls reset() to cleanup old test build environments.
It parses command-line parameters passed by the user.
It calls build_container() to build the docker images used for testing.
It calls run_test() to run individual test scenarios.
Note: variables passed to the main() function are pre-sanitized outside the function.

#### Example

```bash
   If you want to run just test_01 using dependencies installed globally
   ./run-tests.sh 01
   If you want to run all tests using dependencies installed globally
   ./run-tests.sh all
   If you want to run test_02 using dependencies installed via pipenv + pyenv
   pipenv run ./run-tests.sh 02
@see https://pipenv.pypa.io/en/latest/
@see https://github.com/pyenv/pyenv
```

#### Arguments

* **$0** (is): the script's relative location, used to parse out the absolute location by bootstrap().
* **$1** (string): Is either "all" to run all tests or a test number (i.e. 01, 24, etc.).
* **$2** (string): (If used) is the "--no-destroy" flag used to preserve test envs after a test run.

#### Exit codes

* **0**: If all test builds and runs are successful, and all bashtests pass.
* **1**: If any test builds or runs fail, or if any bashtests fail.

