# activate

A helper script that works similarly to a virtualenv 'activate' command

## Overview

This script is the entrypoint for repoman local users and developers.
It sets the repoman top-level directory variable ($tld) for the repoman-launcher.
It also sanitizes and passes along $1 to the launcher.
When repoman containers launch, the $PS1 reads: (repoman-$env) $user@$host $pwd.
This is a similar setup to a python virtualenv, hence the name "activate"

## Index

* [dep_check()](#dep_check)
* [bootstrap()](#bootstrap)
* [main()](#main)

### dep_check()

This function performs a preliminary dependency check.
Any non-bash-builtin dep in the path should be added to this function.

_Function has no arguments._

#### Exit codes

* **0**: If all listed deps are present in $PATH
* **1**: If any listed dep is not present in $PATH; script exits after checking all.

### bootstrap()

The bootstrap funtion sets the 'tld' variable used by scripts called by this one.
It will also 'cd' into that directory to ensure all references to files/dirs are consistent.

#### Arguments

* **$0** (string): The filename of the script, as called by the user

#### Exit codes

* **0**: If successful.
* **1**: If failed; does not 'return 1', fatal errors 'exit 1', exiting script entirely

### main()

This funciton is the entrypoint to the script.
It sets the $tld variable for src/host/repoman-launcher.sh
Calls src/host/repoman-launcher.sh

#### Arguments

* **$1** (string): Is the arg passed to src/host/repoman-launcher.sh

#### Exit codes

* **0**: If all functions called by main() are successful
* **1**: If any function, including main itself, fails.

