# repoman-launcher.sh

A helper script for launching a local instance of repoman

## Overview

This script is used to launch a local instance of repoman.
Common use cases for wanting to launch repoman locally include:
- Encrypting secrets files for inclusion in a repoman-config repository
- Decrypting secrets files from an existing repoman-config repository for reading/modification
- Manually rotating the ansible-vault password file used to encrypt/decrypt secrets
- Manually rotating deploy keypairs associated with a remote repository
- Creating and applying repository configuration-as-code from your local machine to remote repositories

## Index

* [no_root()](#no_root)
* [bootstrap()](#bootstrap)
* [dep_check()](#dep_check)
* [init()](#init)
* [build_and_launch()](#build_and_launch)
* [main()](#main)

### no_root()

This function ensures that the user is not executing the script as root or with sudo.

_Function has no arguments._

#### Exit codes

* **0**: If user is non-root.
* **1**: If user is root or effectively root (via sudo).

### bootstrap()

The bootstrap funtion sets the 'scriptdir' variable for other functions to use.
It will also 'cd' into that directory to ensure all references to files/dirs are consistent.

#### Arguments

* **$0** (string): The filename of the script, as called by the user

#### Exit codes

* **0**: If successful
* **1**: If failed; does not 'return 1', fatal errors 'exit 1', exiting script entirely

### dep_check()

This function performs a preliminary dependency check.
Any non-bash-builtin dep in the path should be added to this function.

_Function has no arguments._

#### Exit codes

* **0**: If all listed deps are present in $PATH
* **1**: If any listed dep is not present in $PATH; script exits after checking all.

### init()

This function creates (or checks for) required credentials and configs.

#### Exit codes

* **0**: If all configs and creds are present
* **1**: If any configs or creds are missing and cannot be generated

### build_and_launch()

This function builds and launches the user-selected environment.
It either launches a development or local production environment.

#### Arguments

* **$1** (string): Is the environment name. This is optional. The default is "local".

### main()

This function is the entrypoint to the script.
It builds either:
- repoman_devel (used for local development workflow)
- repoman_local (used for local production workflow)

#### Arguments

* **$1** (string): If "dev" (or a variation of it), script will launch a development instance, not a local production one

#### Exit codes

* **0**: If all functions called by main() are successful and return 0.
* **1**: If any function fails; functions should exit the script on fatal errors. Main exits 1 if unhandled errors encountered.

