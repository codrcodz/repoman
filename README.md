# repoman

## Purpose

Repoman is a tool for managing your Gitlab repositories using configuration-as-code. It allows you to:

- Generate configuration deployment plans
- Dryrun deployment plans
- Deploy deployment plans
- Encrypt secret variables for inclusion in deployment plans
- Decrypt secret variables from previous deployment plans

## Quickstart

This is a TL;DR version of the Feature Tour... for the impatient crowd.

- Make new gitlab group for testing, (named whatever you like), and a project called "repoman-quickstart" inside it
- Generate a personal access token and place it in `~/.config/repoman/GITLAB_TOKEN` on your local machine
- Run the following commands **_after_** updating the value of `gitlab_group` on the first line
- `export gitlab_group="put_your_gitlab_group_url_slug_here"`
- `git clone https://gitlab.com/dreamer-labs/repoman/repoman.git` && \
  `cd repoman/` && \
  `cp -pR example-repoman-data/ repoman-data/` && \
  `sed -i "s%placeholder%$gitlab_group%g" ./repoman-data/configs/config.yml.j2` && \
  `sed -i "s%placeholder%$gitlab_group%g" ./repoman-data/configs/vars.yml` && \
  `./activate`
- Wait for the `repoman-local` environment to build and launch (takes approximately 1-2 minutes)
- `repoman encrypt-secrets` && \
  `repoman deploy-configs`

## Feature Tour

This is a more verbose version of the quickstart with a few extra steps thrown in to illustrate all the basic features of repoman.

### Create Gitlab personal access token

In order to apply configuration-as-code to a repo, a personal access token is needed.

- On your local machine, open a web browser
- Login to <https://gitlab.com>
- Click your profile avatar/photo in the top, right-hand corner
- In the drop-down menu, click _Preferences_
- On the profile preferences screen, click _Access Tokens_
- On the access token screen, create a new token with all available permissions
- Copy the token value to `~/.config/repoman/GITLAB_TOKEN` on your local machine
- Set the permissions to prevent access by other users (i.e. `chmod 0600`)

### Create a "repoman-quickstart" repo

In order to test repoman, you need a test organization and repo to apply configuration-as-code to.

- Login to <https://gitlab.com>
- On the homescreen, click _Menu_ in the top, left corner
- From the drop-down menu, select _Groups_ > _Create group_
- On the next screen, select _New Group_
- On the group creation screen, set `Group name` and `Group URL` to whatever you like
  - For the remaining fields, the default values are fine
- On the new group's main page, click the _New Project_ button
- On the next screen, select _Create a Blank Project_
- On the project creation screen, create a project under your user with the `repoman-quickstart` slug
- It should be under your new group, like this: <https://gitlab.com/yournewgroup/repoman-quickstart>

### Install repoman

- On your local machine, navigate to your preferred working directory, then clone `repoman`
- `git clone https://gitlab.com/dreamer-labs/repoman/repoman.git`
- `cd repoman/`

### Configure repoman-data/ directory

Repoman requires some configuration-as-code in order to apply it to a Gitlab repository.
Luckily, some working sample data is already provided in this repository.

- `cp -pR example-repoman-data/ repoman-data/`
- Replace "your_test_group_here" with the group URL slug of your new test group in the `sed` commands below; then run each `sed`
  - Note: the slug is the tail end of the `Group URL`, which is the part after the last slash in: <https://gitlab.com/your_test_group_here> )
- `sed -i 's%placeholder%your_test_group_here%g' ./repoman-data/configs/config.yml.j2`
- `sed -i 's%placeholder%your_test_group_here%g' ./repoman-data/configs/vars.yml`

### Activate the repoman-local environment

Repoman can be used in GitlabCI pipelines (repoman-remote) to automate configuration-as-code deployments;
however, this quickstart focuses on applying configuration-as-code from a local machine.

- `./activate`
- Install any missing dependencies, and rerun `./activate` (if needed)
- The `repoman-local` environment has a unique shell prompt, indicating that the environment has been entered

### Use repoman-local to encrypt a test secret

Prior to pushing secrets to a repository for safe keeping, the secrets must first be encrypted.
Repoman uses `ansible-vault` to encrypt secrets files.
A sample secrets file called "testsecret" is provided in the `repoman-data/secrets/unencrypted/` directory.
Inside the `repoman-local` environment, run the following command to encrypt all secrets files in this directory:

- `repoman encrypt-secrets`

Secrets will be encrypted using an `ansible-vault` password file that was auto-generated for you.
The password file is located in the `~/.config/repoman/ANSIBLE_VAULT_PASS` file. Keep it safe.
Secrets are not encrypted in place, the encrypted versions are placed in the `repoman-data/secrets/encrypted/` directory.

### Decrypting a secret

If you have the `ansible-vault` password used to encrypt a secret, that same password can be used to decrypt it.
If you are no longer in `repoman-local` environment, activate it again now.

- `./activate`

Then clear out the `secrets/unencrypted/` directory, (just for demonstration purposes), this is not needed in production.

- `rm /etc/repoman/secrets/unencrypted/*`

Then run the decrypt command inside the environment.

- `repoman decrypt-secrets`

If you previously ran the `repoman encrypt-secrets` command,
a decrypted copy of the `testsecret` should now be in the `/etc/repoman/secrets/unencrypted/` directory.

Note: If you accidentally decrypt (or encrypt) secrets when a file of the same name already exists in the target directory,
do not worry; a copy of the existing file is made with a timestamp appended to the filename and placed alongside the original.

### Generating configuration-as-code and executing a dryrun deploy

- `./activate`
- `repoman dryrun-deploy`
- `cat /etc/repoman/tempdir/config.yml`

Stdout inside the `repoman-local` should now show output from the dryrun and the contents of the config file that was generated.

### Deploying configuration-as-code

- `./activate`
- `repoman deploy-configs`
- cat /etc/repoman/tempdir/config.yml`

The stdout from this set of commands should look very similar to the previous one,
except the configs displayed are actually deployed.

## Usage

In addtion to the features listed in the Quickstart, there are other features that may be utilized.

### Pushing encrypted secrets to a remote repoman-data repository

In a shared Gitlab organization, a peer-review should be conducted before applying configurations.
This requires you to create a merge request that includes your local copy of the configs, along with the encrypted secrets.


### Decrypting existing secrets from an existing `repoman-data` repository

If you are not the first person to encrypt secrets and place them in the `repoman-data` repository,
you will need a copy of the `ansible-vault` password file used by other maintainers to decrypt secrets they generated.
That secret would have to be fetched from the repository like so:

- Login to <https://gitlab.com/>
- Retreive the secret from the project's settings menu under Settings > CI/CD > Variables
- Find the `ANSIBLE_VAULT_PASS` variable's value and copy it to `~/.config/repoman/ANSIBLE_VAULT_PASS` on the local machine

After you have the password needed to decrypt the secrets, the secrets will need to be pulled down to your local machine.

- `cd  repoman`
- `git clone https://gitlab.com/yourusernamehere/repoman-data/`

Next, activate the environment, and then execute the `repoman decrypt-secrets` command.

- `./activate`
- `repoman decrypt-secrets`

The decrypted secrets should now be in the `/etc/repoman/secrets/unencrypted` directory.
The same files are visible directly on the local machine (outside of the `repoman-local` environment).
