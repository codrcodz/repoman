#!/usr/bin/env bash
#
# @name run-tests.sh
# @brief This script acts as a test framework for repoman
# @description
#    This test framework uses docker-compose to run various test scenarios.
#    Tests environments are defined in the "test_env.yml" file in each test scenario directory.
#    The "test_env.yml" file is a docker-compose file. Secrets are defined in it as well.
#    Also included is a "mk-test.sh" script that aids the user in automatic test creation.
#    This framework uses tests written in bashtest format.
#    The bashtest perform functional evaluation and integration testing, NOT unit testing.
#    Configuration files mounted by docker-compose are stored in each test_* directory.
#    These directories also contain a bashtest/ subdir with a bashtest per file.
# @see https://github.com/pahaz/bashtest#typical-use-case
# @see https://www.rockyourcode.com/using-docker-secrets-with-docker-compose/
# @see https://docs.docker.com/compose/reference/
# @see https://docs.docker.com/compose/

# This line is required to make some of the globbing work properly in certain functions
shopt -s nullglob extglob

# @description
#    This function autoformats, lints, and generates docs for this script (and others).
#    The autoformatter rules are configured with the .editorconfig in the tld of this repo.
# @arg $0 string Tells the function the location of the script so each tool can parse it.
# @exitcode 0 If autoformatting, linting, and autodoc generation all pass.
# @exitcode 1 Once the first one fails. No warnings (return 1). Fatal errors (exit 1).
# @see https://github.com/reconquest/shdoc#features
# @see https://github.com/koalaman/shellcheck#how-to-use
# @see https://github.com/mvdan/sh
# @see https://editorconfig.org/
self_heal() {
  local heal_fail &&
    heal_fail=0 &&
    for file in "$scriptname" \
      ../src/host/repoman-launcher.sh \
      ./mk-test.sh \
      ../activate; do
      {
        if [[ -f "$file" ]]; then
          {
            shfmt -w "$file" &&
              shellcheck "$file" &&
              shdoc < "$file" > ../docs/"${file##*/}".md
          } ||
            {
              echo -e "\n[WARN] The script (${file##*/}) failed autoformatting, linting, and/or autodocs." 1>&2
              heal_fail=1
            }
        else
          echo -e "\n[FAIL] Could not detect relative location of this script; exiting.\n" 1>&2
          exit 1
        fi &&
          if [[ "$heal_fail" == "1" ]]; then
            echo -e "\n[FAIL] One or more scripts failed autoformatting, linting, and/or autodocs; exiting." 1>&2
            exit 1
          fi
      }
    done
}

# @description
#    The bootstrap funtion sets the 'scriptdir' variable for other functions to use.
#    It will also 'cd' into that directory to ensure all references to files/dirs are consistent.
# @arg $0 string The filename of the script, as called by the user
# @exitcode 0 If successful
# @exitcode 1 If failed; does not 'return 1', fatal errors 'exit 1', exiting script entirely
bootstrap() {
  {
    unset scriptdir &&
      declare -g scriptdir &&
      scriptdir="$(dirname "$(realpath "$0")")" &&
      readonly scriptdir &&
      export scriptdir &&
      if [[ -n "$scriptdir" ]] && [[ -d "$scriptdir" ]]; then
        cd "$scriptdir"
      else
        echo -e "\n[FAIL] Failed to detect script directory; exiting.\n" 1>&2
        exit 1
      fi
  } ||
    {
      echo -e "\n[FAIL] Failed to change directories into $0 script dir; exiting.\n" 1>&2
      exit 1
    }
  {
    unset scriptname &&
      declare -g scriptname &&
      scriptname="$(basename "$0")" &&
      readonly scriptname &&
      export scriptname &&
      if [[ ! -f "$scriptname" ]]; then
        echo -e "\n[FAIL] Failed to detect scriptname; exiting.\n" 1>&2
        exit 1
      fi
  }
}

# @description
#    This function resets the test build directories prior to each test run.
#    It does not perform any of this work itself.
#    It loops through the test scenario dirnames and passes test numbers to the 'destroy' func.
# @noargs
# @see destroy()
reset() {
  for test in test_*; do
    destroy "${test//test_/}"
  done
}

# @description
#    This function prepares the build environments used to run tests.
#    Each test run, for each scenario, gets its own environment and configs.
#    Environments consist of the entry in "test_env.yml" file
#    Configs come from the contents of "tempdir/", "configs/", and "secrets/" subdirs.
#    If the subdirs are not already present in the test_* dir, they will be created.
#    This ensures each test env has /etc/repoman/{configs,secrets,tempdir} mounted.
# @arg $1 integer Is the number of the test scenario being prepared.
# @exitcode 0 If successful.
# @exitcode 1 If failed; exits, does not return.
# @see bootstrap()
prepare() {
  {
    mkdir -p "$scriptdir/test_$1/build/"
    for subdir in configs secrets tempdir; do
      if [[ -d "$scriptdir/test_$1/$subdir" ]]; then
        cp -pR "$scriptdir/test_$1/$subdir/" "$scriptdir/test_$1/build/$subdir/" ||
          {
            echo -e "\n[FAIL] Failed to copy test_$1/build/$subdir; exiting.\n" 1>&2
            exit 1
          }
      else
        mkdir -p "$scriptdir/test_$1/build/$subdir/" ||
          {
            echo -e "\n[FAIL] Failed to make test_$1/build/$subdir; exiting.\n" 1>&2
            exit 1
          }
      fi
    done
  }
}

# @description
#    This function destroys test build directories.
#    It is called by reset() when a new test run begins.
#    It is called by run_test() when a test scenario passes.
#    It is skipped by passing the "--no-destroy" function to main().
# @arg $1 integer Is the test scenario number whose build dir will be removed.
# @exitcode 0 If successful.
# @exitcode 1 If failed. Exits on fail, does not return.
# @see reset()
# @see run_test()
# @see main()
destroy() {
  rm -fr "$scriptdir/test_$1/build/" ||
    {
      echo -e "\n[FAIL] Failed to remove build directory for test $1; must remove before retrying.\n" 1>&2
      exit 1
    }
}

# @description
#    This function performs most of the logic and flow of the script.
#    It calls prepare() to prep test environments.
#    It executes docker-compose once environments are prepped/resent.
#    It runs `bashtest` once docker-compose exits (if successful).
#    It calls destroy() to cleanup environments of successful tests.
#    It skips cleanup if main() passes "--no-destroy" or a test fails.
#    When failures occur, this function warns and returns, but does not exit.
#    This gives other tests a chance to run, instead of the script exiting on first test failure.
# @arg $1 integer Is the test scenario number to be executed.
# @exitcode 0 If all bashtests pass for a given test scenario
# @exitcode 1 If any bashtests with a scenario fail, or test scenario is not found; returns, does not exit.
# @see prepare()
# @see destroy()
# @see main()
# @see https://github.com/pahaz/bashtest#typical-use-case
# @see https://docs.docker.com/compose/
run_test() {
  unset failures
  local failures
  if [[ -d "test_$1" ]]; then
    {
      test_dep_check "$1" &&
        prepare "$1" &&
        docker-compose -f "$scriptdir/test_$1"/test_env.yml run --rm "test_$1"
    } ||
      {
        echo -e "\n[WARN] Failed to launch test environment." 1>&2
        return 1
      }
    for bashtest in "$scriptdir/test_$1"/bashtests/*.bashtest; do
      {
        bashtest "$bashtest"
      } ||
        {
          echo -e "\n[WARN] Bashtest failed: $bashtest" 1>&2
          failures=1
        }
    done &&
      if [[ "$2" != "--no-destroy" ]] && [[ "$failures" != "1" ]]; then
        destroy "$1"
      fi
  else
    echo -e "\n[WARN] Could not find test: test_$1." 1>&2
    return 1
  fi
  if [[ "$failures" == "1" ]]; then return 1; fi
}

# @description
#    This function performs a test case dependency check.
#    Any non-bash-builtin test deps in the path should be added to a test_*/bashtests/deps.txt file.
#    This function is called by run_test() on a per-test basis.
#    The "|| [[ -n "$dep ]]" code is there for instances where the deps file does not end in a new line.
# @arg 1 integer Is the test number to check the test deps file for
# @exitcode 0 If all listed test deps are present in $PATH
# @exitcode 1 If any listed test dep is not present in $PATH; script will 'return 1'.
# @see run_test()
# @see https://stackoverflow.com/questions/12916352/shell-script-read-missing-last-line/12919766#12919766
test_dep_check() {
  local missing_deps &&
    missing_deps=0 &&
    while read -r dep || [[ -n "$dep" ]]; do
      hash "$dep" ||
        {
          echo -e "\n[WARN] $dep not found in path (required by test_$1)." 1>&2
          missing_deps=1
        }
    done < "$scriptdir/test_$1/bashtests/deps.txt"
  if [[ "$missing_deps" == "1" ]]; then
    return 1
  fi
}
# @description
#    This function builds the docker containers for the test environments.
#    It uses a multi-stage build process to build the environment, from the same Dockerfile.
#    When the user passes the "all" option to main(), these are built without using the cache.
#    This ensures that when repoman dev's run their final tests, no old configs are cached in the test images.
#    When repoman devs pass a test number to main(), cached image builds are used to speed up dev/test work.
# @arg $1 string Is the "--no-cache" flag selectively passed from main()
# @exitcode 0 If succcessful.
# @exitcode 1 If failed. Exits on failed builds; does not return 1 to main().
# @see https://docs.docker.com/engine/reference/builder/
# @see https://docs.docker.com/compose/reference/build/
# @see https://docs.docker.com/develop/develop-images/multistage-build/
# @see https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#leverage-build-cache
build_container() {
  if [[ "$1" == "--no-cache" ]]; then
    {
      docker-compose -f ../src/host/docker-compose.yml build \
        --build-arg LOCAL_USER="$(id -un)" \
        --build-arg LOCAL_UID="$(id -u)" \
        --build-arg LOCAL_GROUP="$(id -gn)" \
        --build-arg LOCAL_GID="$(id -g)" \
        --no-cache \
        repoman_local
    } ||
      {
        echo -e "\n[FAIL] Failed to build container image from cache for testing; exiting.\n" 1>&2
        exit 1
      }
  else
    {
      docker-compose -f ../src/host/docker-compose.yml build \
        --build-arg LOCAL_USER="$(id -un)" \
        --build-arg LOCAL_UID="$(id -u)" \
        --build-arg LOCAL_GROUP="$(id -gn)" \
        --build-arg LOCAL_GID="$(id -g)" \
        repoman_local
    } ||
      {
        echo -e "\n[FAIL] Failed to build container image from cache for testing; exiting.\n" 1>&2
        exit 1
      }
  fi
}

# @description
#    This function performs a preliminary dependency check.
#    Any non-bash-builtin dep in the path should be added to this function.
# @noargs
# @exitcode 0 If all listed deps are present in $PATH
# @exitcode 1 If any listed dep is not present in $PATH; script exits after checking all.
dep_check() {
  {
    local missing_deps &&
      missing_deps=0 &&
      for dep in mv \
        rm \
        realpath \
        id \
        dirname \
        shfmt \
        shellcheck \
        shdoc \
        mkdir \
        pwgen \
        bashtest \
        docker-compose; do
        hash $dep ||
          {
            echo -e "\n[WARN] $dep not found in path." 1>&2
            missing_deps=1
          }
      done
    if [[ "${missing_deps}" == "1" ]]; then
      echo -e "\n[FAIL] Please install missing dependencies and try again; exiting." 1>&2
      exit 1
    fi
  }
}

# @description
#    This function is the main entrypoint into the script.
#    It calls dep_check() to ensure required deps are installed.
#    It calls self_heal() to perform auto-linting/formatting/docs.
#    It calls bootstrap() to setup global vars and cd into the right directory.
#    It calls reset() to cleanup old test build environments.
#    It parses command-line parameters passed by the user.
#    It calls build_container() to build the docker images used for testing.
#    It calls run_test() to run individual test scenarios.
#    Note: variables passed to the main() function are pre-sanitized outside the function.
# @arg $0 is the script's relative location, used to parse out the absolute location by bootstrap().
# @arg $1 string Is either "all" to run all tests or a test number (i.e. 01, 24, etc.).
# @arg $2 string (If used) is the "--no-destroy" flag used to preserve test envs after a test run.
# @exitcode 0 If all test builds and runs are successful, and all bashtests pass.
# @exitcode 1 If any test builds or runs fail, or if any bashtests fail.
# @example
#    If you want to run just test_01 using dependencies installed globally
#    ./run-tests.sh 01
# @example
#    If you want to run all tests using dependencies installed globally
#    ./run-tests.sh all
# @example
#    If you want to run test_02 using dependencies installed via pipenv + pyenv
#    pipenv run ./run-tests.sh 02
# @see https://pipenv.pypa.io/en/latest/
# @see https://github.com/pyenv/pyenv
main() {
  dep_check &&
    bootstrap &&
    self_heal &&
    reset &&
    case "$1" in
    all)
      build_container --no-cache &&
        for test in test_*; do
          {
            run_test "${test//test_/}" "$2" &&
              echo -e "\n[INFO] Test ${test//test_/} passed!"
          } ||
            {
              echo -e "\n[WARN] Test ${test//test_/} failed!" 1>&2
              return 1
            }
        done
      ;;
    +([[:digit:]]))
      build_container ||
        {
          echo -e "\n[FAIL] Failed to bulid container for testing; exiting.\n" 1>&2
          exit 1
        }
      {
        run_test "$1" "$2" &&
          echo -e "\n[PASS] Test $1 passed!"
      } ||
        {
          echo -e "\n[WARN] Test $1 failed!" 1>&2
          return 1
        }
      ;;
    *)
      echo -e "\n[FAIL] Failed to pass 'all' or a test number (i.e. '01', '24', etc) to script; exiting.\n" 1>&2
      exit 1
      ;;
    esac
}

# This block sanitizes inputs, then calls the main function and reports any test failures, (when required).
{
  {
    declare arg_one &&
      arg_one="${1//[^[:alnum:]]/}" &&
      declare arg_two &&
      arg_two="${2//[^[:alnum:]-]/}"
  } ||
    {
      echo -e "\n[FAIL] Failed to properly sanitize script inputs; exiting.\n" 1>&2
      exit 1
    }
  {
    main "${arg_one}" "${arg_two}" &&
      echo -e "\n[PASS] All tests passed; exiting!\n"
    exit 0
  } ||
    {
      echo -e "\n[FAIL] One or more test failures detected; exiting.\n" 1>&2
      exit 1
    }
}
